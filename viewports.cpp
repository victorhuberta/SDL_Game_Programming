#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>

bool init();
bool loadMedia();
SDL_Texture *loadTexture(std::string path);
void destroy();

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

SDL_Window *gWindow = NULL;
SDL_Renderer *gRenderer = NULL;
SDL_Texture *gTexture = NULL;

int main(int argc, char **argv)
{
    if (! init()) {
        printf("Failed to initialize!\n");
        return 1;
    }

    if (! loadMedia()) {
        printf("Failed to load media!\n");
        destroy();
        return 1;
    }

    bool quit = false;
    SDL_Event e;

    while (! quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            }
        }

        SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear(gRenderer);

        SDL_Rect topLeftViewport = {};
        topLeftViewport.x = 0;
        topLeftViewport.y = 0;
        topLeftViewport.w = SCREEN_WIDTH / 2;
        topLeftViewport.h = SCREEN_HEIGHT / 2;
        SDL_RenderSetViewport(gRenderer, &topLeftViewport);
        SDL_RenderCopy(gRenderer, gTexture, NULL, NULL);

        SDL_Rect topRightViewport = {};
        topRightViewport.x = SCREEN_WIDTH / 2;
        topRightViewport.y = 0;
        topRightViewport.w = SCREEN_WIDTH / 2;
        topRightViewport.h = SCREEN_HEIGHT / 2;
        SDL_RenderSetViewport(gRenderer, &topRightViewport);
        SDL_RenderCopy(gRenderer, gTexture, NULL, NULL);

        SDL_Rect bottomViewport = {};
        bottomViewport.x = 0;
        bottomViewport.y = SCREEN_HEIGHT / 2;
        bottomViewport.w = SCREEN_WIDTH;
        bottomViewport.h = SCREEN_HEIGHT / 2;
        SDL_RenderSetViewport(gRenderer, &bottomViewport);
        SDL_RenderCopy(gRenderer, gTexture, NULL, NULL);

        SDL_RenderPresent(gRenderer);
    }

    destroy();
    return 0;
}

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Failed to initialize SDL: %s\n", SDL_GetError());
        success = false;
    } else {

        if (IMG_Init(IMG_INIT_PNG) < 0) {
            printf("Failed to initialize SDL_image: %s\n", IMG_GetError());
            success = false;
        } else {

            gWindow = SDL_CreateWindow("CATE", SDL_WINDOWPOS_UNDEFINED,
                SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT,
                SDL_WINDOW_SHOWN);

            if (gWindow == NULL) {
                printf("Failed to create window: %s\n", SDL_GetError());
                success = false;
            } else {

                gRenderer = SDL_CreateRenderer(gWindow, -1,
                    SDL_RENDERER_ACCELERATED);
                if (gRenderer == NULL) {
                    printf("Failed to create renderer: %s\n", SDL_GetError());
                    success = false;
                } else {

                    SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
                }
            }
        }
    }

    return success;
}

bool loadMedia()
{
    bool success = true;

    gTexture = loadTexture("cat.png");
    if (gTexture == NULL) {
        printf("Failed to load texture!\n");
        success = false;
    }

    return success;
}

SDL_Texture *loadTexture(std::string path)
{
    SDL_Texture *loadedTexture = NULL;
    SDL_Surface *loadedSurface = IMG_Load(path.c_str());

    if (loadedSurface == NULL) {
        printf("Failed to load image '%s': %s\n", path.c_str(), SDL_GetError());
    } else {

        loadedTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
        if (loadedTexture == NULL) {
            printf("Failed to create texture: %s\n", SDL_GetError());
        }

        SDL_FreeSurface(loadedSurface);
    }

    return loadedTexture;
}

void destroy()
{
    if (gTexture != NULL) {
        SDL_DestroyTexture(gTexture);
        gTexture = NULL;
    }

    if (gRenderer != NULL) {
        SDL_DestroyRenderer(gRenderer);
        gRenderer = NULL;
    }

    if (gWindow != NULL) {
        SDL_DestroyWindow(gWindow);
        gWindow = NULL;
    }

    IMG_Quit();
    SDL_Quit();
}
