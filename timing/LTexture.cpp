#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <string>

#include "LTexture.h"

bool LTexture::load_text_from_font(TTF_Font* font,
    std::string text_str, SDL_Color color)
{
    if (renderer == NULL) {
        printf("No renderer found\n");
        return false;
    }

    free();

    SDL_Surface* font_surface = TTF_RenderText_Solid(font,
        text_str.c_str(), color);
    if (font_surface == NULL) {
        printf("Failed to create font surface: %s\n", TTF_GetError());
    } else {

        texture = SDL_CreateTextureFromSurface(renderer, font_surface);
        if (texture == NULL) {
            printf("Failed to create font texture: %s\n", SDL_GetError());
        } else {
            width = font_surface->w;
            height = font_surface->h;
        }

        SDL_FreeSurface(font_surface);
    }

    return texture != NULL;
}

void LTexture::render(int x, int y, SDL_Rect* clip)
{
    if (renderer == NULL) {
        printf("No renderer found\n");
        return;
    }

    SDL_Rect renderQuad = {x, y, width, height};

    if (clip != NULL) {
        renderQuad.w = clip->w;
        renderQuad.h = clip->h;
    }

    SDL_RenderCopy(renderer, texture, clip, &renderQuad);
}

void LTexture::free()
{
    if (texture != NULL) {
        SDL_DestroyTexture(texture);
        texture = NULL;
        width = 0;
        height = 0;
    }
}
