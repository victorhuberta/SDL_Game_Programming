#ifndef _VTIMER_H
#define _VTIMER_H

class VTimer
{
    public:
        VTimer() : start_time {0}, paused_time {0},
            started {false}, paused {false} {}

        void start();
        void stop();
        void pause();
        void resume();

        Uint32 get_ticks();
        double to_seconds() { return get_ticks() / 1000.0; }

        bool is_started() { return started; }
        bool is_paused() { return paused; }

    private:
        Uint32 start_time, paused_time;
        bool started, paused;
};

#endif
