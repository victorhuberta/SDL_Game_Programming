#include <SDL2/SDL.h>
#include "VTimer.h"

void VTimer::start()
{
    if (! started) {
        started = true;
        paused = false;
        start_time = SDL_GetTicks();
        paused_time = 0;
    }
}

void VTimer::stop()
{
    if (started) {
        started = false;
        paused = false;
        start_time = 0;
        paused_time = 0;
    }
}

void VTimer::pause()
{
    if (started && ! paused) {
        paused = true;
        paused_time = SDL_GetTicks() - start_time;
        start_time = 0;
    }
}

void VTimer::resume()
{
    if (started && paused) {
        paused = false;
        start_time = SDL_GetTicks() - paused_time;
        paused_time = 0;
    }
}

Uint32 VTimer::get_ticks()
{
    if (started) {
        if (paused) {
            return paused_time;
        } else {
            return SDL_GetTicks() - start_time;
        }
    }

    return 0;
}
