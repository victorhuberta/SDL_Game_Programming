#ifndef LTEXTURE_H
#define LTEXTURE_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <string>

class LTexture
{
    public:
        LTexture() : texture {NULL}, renderer {NULL}, width {0}, height {0} {}
        ~LTexture() { free(); }

        void set_renderer(SDL_Renderer* r) { renderer = r; }

        bool load_text_from_font(TTF_Font* font,
            std::string text_str, SDL_Color color);

        void render(int x, int y, SDL_Rect* clip = NULL);
        void free();
        int get_width() { return width; }
        int get_height() { return height; }

    private:
        SDL_Texture* texture;
        SDL_Renderer* renderer;
        int width, height;
};

#endif
