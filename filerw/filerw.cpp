#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>

#include "LTexture.h"

void render_data_tex(int data_idx, SDL_Color color);
bool load_data();
bool save_data();
bool init();
bool load_media();
void destroy();

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

const char* SAVE_PATH = "save.bin";
const char* FONT_PATH = "font.ttf";
const int FONT_SIZE = 16;
const std::string TEXT_STR = "Enter data";
const int N_DATA = 10;

SDL_Window* g_window = NULL;
SDL_Renderer* g_renderer = NULL;
TTF_Font* g_font = NULL;
LTexture text;
LTexture data_texs[N_DATA];
Sint32 data[N_DATA] = {};

int main(int argc, char* argv[])
{
    if (! init()) {
        printf("Failed to initialize!\n");
        return 1;
    }

    if (! load_media()) {
        printf("Failed to load media!\n");
        destroy();
        return 1;
    }

    bool quit = false;
    SDL_Event e;

    int data_idx = 0;
    SDL_Color color = {0, 0, 0, 0xFF};
    SDL_Color highlight_color = {0xFF, 0, 0, 0xFF};

    load_data();

    for (int i = 0; i < N_DATA; ++i) {
        data_texs[i].set_renderer(g_renderer);
        render_data_tex(i, color);
    }

    while (! quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            } else if (e.type == SDL_KEYDOWN) {
                switch (e.key.keysym.sym) {
                    case SDLK_UP:
                        render_data_tex(data_idx, color);
                        --data_idx;
                        if (data_idx < 0) {
                            data_idx = N_DATA - 1;
                        }
                        render_data_tex(data_idx, highlight_color);
                        break;
                    case SDLK_DOWN:
                        render_data_tex(data_idx, color);
                        ++data_idx;
                        if (data_idx >= N_DATA) {
                            data_idx = 0;
                        }
                        render_data_tex(data_idx, highlight_color);
                        break;
                    case SDLK_LEFT:
                        --data[data_idx];
                        render_data_tex(data_idx, highlight_color);
                        break;
                    case SDLK_RIGHT:
                        ++data[data_idx];
                        render_data_tex(data_idx, highlight_color);
                        break;
                }
            }
        }

        SDL_SetRenderDrawColor(g_renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear(g_renderer);

        int text_x = (SCREEN_WIDTH - text.get_width()) / 2;
        int text_y = (SCREEN_HEIGHT - text.get_height()) / 3;
        text.render(text_x, text_y);
        for (int i = 0; i < N_DATA; ++i) {
            data_texs[i].render((SCREEN_WIDTH - data_texs[i].get_width()) / 2,
                text_y + text.get_height() + (data_texs[i].get_height() * i));
        }

        SDL_RenderPresent(g_renderer);
    }

    save_data();
    destroy();
    return 0;
}

void render_data_tex(int data_idx, SDL_Color color)
{
    data_texs[data_idx].load_text_from_font(g_font,
        std::to_string(data[data_idx]), color);
}

bool load_data()
{
    SDL_RWops* file = SDL_RWFromFile(SAVE_PATH, "r+b");
    if (file == NULL) {
        printf("Save file doesn't exist: %s\n", SDL_GetError());
        return false;
    } else {
        if (SDL_RWread(file, data, sizeof(Sint32), N_DATA) != N_DATA) {
            printf("Failed to load all data: %s\n", SDL_GetError());
            return false;
        }
    }

    return true;
}

bool save_data()
{
    SDL_RWops* file = SDL_RWFromFile(SAVE_PATH, "w+b");
    if (file != NULL) {
        if (SDL_RWwrite(file, data, sizeof(Sint32), N_DATA) != N_DATA) {
            printf("Failed to save all data: %s\n", SDL_GetError());
            return false;
        }

        SDL_RWclose(file);
    } else {
        printf("Failed to open save file: %s\n", SDL_GetError());
        return false;
    }

    return true;
}

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Failed to initialize SDL: %s\n", SDL_GetError());
        success = false;
    } else {
        
        g_window = SDL_CreateWindow("FONT DEMO", SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT,
            SDL_WINDOW_SHOWN);
        if (g_window == NULL) {
            printf("Failed to create window: %s\n", SDL_GetError());
            success = false;
        } else {

            g_renderer = SDL_CreateRenderer(g_window, -1,
                SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
            if (g_renderer == NULL) {
                printf("Failed to create renderer: %s\n", SDL_GetError());
                success = false;
            } else {

                if (TTF_Init() == -1) {
                    printf("Failed to initialize SDL_ttf: %s\n", TTF_GetError());
                    success = false;
                }
            }
        }
    }

    return success;
}

bool load_media()
{
    bool success = true;

    g_font = TTF_OpenFont(FONT_PATH, FONT_SIZE);
    if (g_font == NULL) {
        printf("Failed to open font: %s\n", TTF_GetError());
        success = false;
    } else {

        text.set_renderer(g_renderer);

        SDL_Color color = {0, 0, 0};
        if (! text.load_text_from_font(g_font, TEXT_STR, color)) {
            printf("Failed to load text from font!\n");
            success = false;
        }
    }

    return success;
}

void destroy()
{
    text.free();

    if (g_font != NULL) {
        TTF_CloseFont(g_font);
        g_font = NULL;
    }

    if (g_renderer != NULL) {
        SDL_DestroyRenderer(g_renderer);
        g_renderer = NULL;
    }

    if (g_window != NULL) {
        SDL_DestroyWindow(g_window);
        g_window = NULL;
    }

    TTF_Quit();
    SDL_Quit();
}
