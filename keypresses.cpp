#include <SDL2/SDL.h>
#include <stdio.h>
#include <string>

bool init();
bool loadMedia();
SDL_Surface *loadSurface(std::string path);
void destroy();

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

enum KeyPressSurfaces {
    KEY_PRESS_SURFACE_DEFAULT,
    KEY_PRESS_SURFACE_UP,
    KEY_PRESS_SURFACE_DOWN,
    KEY_PRESS_SURFACE_LEFT,
    KEY_PRESS_SURFACE_RIGHT,
    N_KEY_PRESS_SURFACES
};

const std::string BMP_PATHS[N_KEY_PRESS_SURFACES] = {
    "04_key_presses/press.bmp",
    "04_key_presses/up.bmp",
    "04_key_presses/down.bmp",
    "04_key_presses/left.bmp",
    "04_key_presses/right.bmp",
};

SDL_Window *gWindow = NULL;
SDL_Surface *gMainSurface = NULL;
SDL_Surface *gSurfaces[N_KEY_PRESS_SURFACES];
SDL_Surface *gCurrentSurface = NULL;

int main(int argc, char **argv)
{
    bool quit = false;

    if (! init()) {
        printf("Failed to initialize!\n");
        return 1;
    }

    if (! loadMedia()) {
        printf("Failed to load media!\n");
        destroy();
        return 1;
    }

    SDL_Event e;
    gCurrentSurface = gSurfaces[KEY_PRESS_SURFACE_DEFAULT];
    while (! quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            } else if (e.type == SDL_KEYDOWN) {
                switch (e.key.keysym.sym) {
                    case SDLK_UP:
                        gCurrentSurface = gSurfaces[KEY_PRESS_SURFACE_UP];
                        break;
                    case SDLK_DOWN:
                        gCurrentSurface = gSurfaces[KEY_PRESS_SURFACE_DOWN];
                        break;
                    case SDLK_LEFT:
                        gCurrentSurface = gSurfaces[KEY_PRESS_SURFACE_LEFT];
                        break;
                    case SDLK_RIGHT:
                        gCurrentSurface = gSurfaces[KEY_PRESS_SURFACE_RIGHT];
                        break;
                    default:
                        gCurrentSurface = gSurfaces[KEY_PRESS_SURFACE_DEFAULT];
                        break;
                }
            }
        }

        SDL_BlitSurface(gCurrentSurface, NULL, gMainSurface, NULL);
        SDL_UpdateWindowSurface(gWindow);
    }

    destroy();
    return 0;
}

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Failed to initialize SDL: %s\n", SDL_GetError());
        success = false;
    }

    gWindow = SDL_CreateWindow("What's up!?", SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT,
        SDL_WINDOW_SHOWN);
    if (gWindow == NULL) {
        printf("Failed to create window: %s\n", SDL_GetError());
        success = false;
    } else {
        gMainSurface = SDL_GetWindowSurface(gWindow);
    }

    return success;
}

bool loadMedia()
{
    bool success = true;

    for (int i = KEY_PRESS_SURFACE_DEFAULT; i < N_KEY_PRESS_SURFACES; ++i) {
        SDL_Surface *imgSurface = loadSurface(BMP_PATHS[i]);
        if (imgSurface == NULL) {
            printf("Failed to load media: %s\n", SDL_GetError());
            success = false;
        }
        gSurfaces[i] = imgSurface;
    }

    return success;
}

SDL_Surface *loadSurface(std::string path)
{
    SDL_Surface *imgSurface = SDL_LoadBMP(path.c_str());
    if (imgSurface == NULL) {
        printf("Failed to load image '%s': %s\n", path.c_str(), SDL_GetError());
    }
    return imgSurface;
}

void destroy()
{
    for (int i = KEY_PRESS_SURFACE_DEFAULT; i < N_KEY_PRESS_SURFACES; ++i) {
        if (gSurfaces[i] != NULL) {
            SDL_FreeSurface(gSurfaces[i]);
            gSurfaces[i] = NULL;
        }
    }
    gCurrentSurface = NULL;

    SDL_DestroyWindow(gWindow);
    gWindow = NULL;
    gMainSurface = NULL;

    SDL_Quit();
}
