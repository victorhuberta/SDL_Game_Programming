#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <exception>
#include <string>
#include <stdio.h>

class NoRenderer: public std::exception
{
    public:
        const char* what() const noexcept { return "No renderer specified."; }
} no_renderer;

class LTexture
{
    public:
        LTexture() : texture {NULL}, width {0}, height {0} {} 
        ~LTexture() { free(); }

        bool load_from_file(std::string path);
        void set_renderer(SDL_Renderer* r) { renderer = r; }
        void render(int x, int y, SDL_Rect* clip = NULL);
        void free();

        int get_width() const { return width; }
        int get_height() const { return height; } 

    private:
        SDL_Texture* texture;
        SDL_Renderer* renderer;
        int width, height;
};

bool LTexture::load_from_file(std::string path)
{
    if (renderer == NULL) {
        throw no_renderer;
    }

    free();

    SDL_Texture* loaded_texture = NULL;
    SDL_Surface* loaded_surface = IMG_Load(path.c_str());
    if (loaded_surface == NULL) {
        printf("Failed to load image '%s': %s\n", path.c_str(), IMG_GetError());
    } else {

        SDL_SetColorKey(loaded_surface, SDL_TRUE,
            SDL_MapRGB(loaded_surface->format, 0x00, 0xFF, 0xFF));

        loaded_texture = SDL_CreateTextureFromSurface(renderer, loaded_surface);
        if (loaded_texture == NULL) {
            printf("Failed to create texture: %s\n", SDL_GetError());
        } else {
            width = loaded_surface->w;
            height = loaded_surface->h;
        }

        SDL_FreeSurface(loaded_surface);
    }

    texture = loaded_texture;
    return texture != NULL;
}

void LTexture::render(int x, int y, SDL_Rect* clip)
{
    if (renderer == NULL) {
        throw no_renderer;
    }

    SDL_Rect renderQuad {x, y, width, height};

    if (clip != NULL) {
        renderQuad.w = clip->w;
        renderQuad.h = clip->h;
    }

    SDL_RenderCopy(renderer, texture, clip, &renderQuad);
}

void LTexture::free()
{
    if (texture != NULL) {
        SDL_DestroyTexture(texture);
        texture = NULL;
        width = 0;
        height = 0;
    }
}

bool init();
void destroy();

const char* WINDOW_TITLE = "CLIP RENDERING";
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

SDL_Window* g_window = NULL;
SDL_Renderer* g_renderer = NULL;

const std::string SPRITESHEET_PATH = "sprites.png";
const int CIRCLE_WIDTH = 100;
const int CIRCLE_HEIGHT = 100;
enum Circles {
    RED_CIRCLE,
    GREEN_CIRCLE,
    YELLOW_CIRCLE,
    BLUE_CIRCLE,
    N_CIRCLES
};
SDL_Rect clips[N_CIRCLES];
LTexture sprite_sheet;

int main(int argc, char* argv[])
{
    if (! init()) {
        printf("Failed to initialize!\n");
        destroy();
        return 1;
    }

    sprite_sheet.set_renderer(g_renderer);

    bool success = false;
    try {
        success = sprite_sheet.load_from_file(SPRITESHEET_PATH);
        if (! success) {
            printf("Failed to load sprite sheet!\n");
            destroy();
            return 1;
        }
    } catch (NoRenderer& e) {
        printf("NoRenderer: %s\n", e.what());
        destroy();
        return 1;
    }

    int x = 0, y = 0;
    for (int i = RED_CIRCLE; i < N_CIRCLES; ++i) {
        clips[i].x = x;
        clips[i].y = y;
        clips[i].w = CIRCLE_WIDTH;
        clips[i].h = CIRCLE_HEIGHT;

        x += CIRCLE_WIDTH;
        if (x >= sprite_sheet.get_width()) {
            x = 0;
            y += CIRCLE_HEIGHT;
        }
    }

    bool quit = false;
    SDL_Event e;

    while (! quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            }
        }

        SDL_SetRenderDrawColor(g_renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear(g_renderer);

        sprite_sheet.render(0, 0, &clips[RED_CIRCLE]);

        sprite_sheet.render(SCREEN_WIDTH - clips[GREEN_CIRCLE].w, 0,
            &clips[GREEN_CIRCLE]);

        sprite_sheet.render(0, SCREEN_HEIGHT - clips[YELLOW_CIRCLE].h,
            &clips[YELLOW_CIRCLE]);

        sprite_sheet.render(SCREEN_WIDTH - clips[BLUE_CIRCLE].w,
            SCREEN_HEIGHT - clips[BLUE_CIRCLE].h, &clips[BLUE_CIRCLE]);

        SDL_RenderPresent(g_renderer);
    }

    return 0;
}

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Failed to initialize SDL: %s\n", SDL_GetError());
        success = false;
    } else {
        g_window = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT,
            SDL_WINDOW_SHOWN);
        if (g_window == NULL) {
            printf("Failed to create window: %s\n", SDL_GetError());
            success = false;
        } else {
            g_renderer = SDL_CreateRenderer(g_window, -1, SDL_RENDERER_ACCELERATED);
            if (g_renderer == NULL) {
                printf("Failed to create renderer: %s\n", SDL_GetError());
                success = false;
            }
        }
    }

    if (IMG_Init(IMG_INIT_PNG) < 0) {
        printf("Failed to initialize SDL_image: %s\n", IMG_GetError());
        success = false;
    }

    return success;
}

void destroy()
{
    sprite_sheet.free();

    if (g_renderer != NULL) {
        SDL_DestroyRenderer(g_renderer);
        g_renderer = NULL;
    }

    if (g_window != NULL) {
        SDL_DestroyWindow(g_window);
        g_window = NULL;
    }

    IMG_Quit();
    SDL_Quit();
}
