#ifndef _DOT_H
#define _DOT_H

#include <SDL2/SDL.h>
#include <exception>
#include <string>
#include "LTexture.h"

class Dot
{
    public:
        Dot(SDL_Renderer* r, std::string path);
        ~Dot() { destroy(); }

        static const int DOT_WIDTH = 20;
        static const int DOT_HEIGHT = 20;
        static const int DOT_VEL = 10;

        void handle_event(SDL_Event& e);
        void move(int max_x, int max_y);
        void render();
        int get_x() { return x; }
        int get_y() { return y; }
        void destroy() { dot_tex.free(); }

    private:
        LTexture dot_tex;

        int x, y;
        int vel_x, vel_y;
};

class FailedToCreateDot: public std::exception
{
    public:
        FailedToCreateDot(std::string m) : err_msg {m} {}

        virtual const char* what() const noexcept
        {
            return err_msg.c_str();
        }

    private:
        std::string err_msg;
};

#endif
