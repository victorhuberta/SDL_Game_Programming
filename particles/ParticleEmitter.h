#ifndef _PARTICLE_EMITTER_H
#define _PARTICLE_EMITTER_H

#include <SDL2/SDL.h>
#include <string>
#include "Dot.h"
#include "Particle.h"

const int N_PARTICLES = 4;
const int N_COLOR_PARTICLES = 3;
const std::string COLOR_PARTICLE_PATHS[N_COLOR_PARTICLES] = {
    "red.bmp", "green.bmp", "blue.bmp"
};
const std::string GLOW_PARTICLE_PATH = "glow.bmp";

class ParticleEmitter
{
    public:
        ParticleEmitter(SDL_Renderer* r, std::string ep) : entity {r, ep},
            renderer {r} {}
        ~ParticleEmitter() { destroy(); }

        bool init();
        void update();
        void render();
        void handle_event(SDL_Event& e) { entity.handle_event(e); };
        void move_entity(int max_x, int max_y) { entity.move(max_x, max_y); }
        void destroy();

    private:
        Dot entity;
        Particle particles[N_COLOR_PARTICLES];
        Particle glow_particle;

        SDL_Renderer* renderer;
};

#endif
