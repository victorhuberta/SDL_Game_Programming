#ifndef _PARTICLE_H
#define _PARTICLE_H

#include <SDL2/SDL.h>
#include <string>
#include "LTexture.h"

class Particle
{
    public:
        Particle();
        ~Particle() { destroy(); }
        
        void set_renderer(SDL_Renderer* r) { tex.set_renderer(r); }
        bool load_from_file(const std::string path);
        void render();
        bool is_dead() { return dead; }
        void revive() { dead = false; life = rand() % N_MAX_LIVES; }
        int get_x() { return x; }
        int get_y() { return y; }
        void set_xy(int x_in, int y_in) { x = x_in; y = y_in; }
        void set_x(int x_in) { x = x_in; }
        void set_y(int y_in) { y = y_in; }
        void destroy() { tex.free(); }

    private:
        LTexture tex;

        int life;
        int x, y;
        bool dead;

        const int N_MAX_LIVES = 12;
};

#endif
