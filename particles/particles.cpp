#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include "ParticleEmitter.h"

bool init();
bool load_media();
void destroy();

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

SDL_Window* g_window = NULL;
SDL_Renderer* g_renderer = NULL;

int main(int argc, char* argv[])
{
    if (! init()) {
        printf("Failed to initialize!\n");
        return 1;
    }

    if (! load_media()) {
        printf("Failed to load media!\n");
        destroy();
        return 1;
    }

    bool quit = false;
    SDL_Event e;
    ParticleEmitter pe {g_renderer, "dot.bmp"};

    if (! pe.init()) {
        printf("Failed to initialize ParticleEmitter\n");
        destroy();
        return 1;
    }

    while (! quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            }

            pe.handle_event(e);
        }

        pe.move_entity(SCREEN_WIDTH, SCREEN_HEIGHT);

        SDL_SetRenderDrawColor(g_renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear(g_renderer);

        pe.update();
        pe.render();

        SDL_RenderPresent(g_renderer);
    }

    pe.destroy();
    destroy();
    return 0;
}

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Failed to initialize SDL: %s\n", SDL_GetError());
        success = false;
    } else {
        
        g_window = SDL_CreateWindow("FONT DEMO", SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT,
            SDL_WINDOW_SHOWN);
        if (g_window == NULL) {
            printf("Failed to create window: %s\n", SDL_GetError());
            success = false;
        } else {

            g_renderer = SDL_CreateRenderer(g_window, -1,
                SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
            if (g_renderer == NULL) {
                printf("Failed to create renderer: %s\n", SDL_GetError());
                success = false;
            } else {
                int img_flags = IMG_INIT_PNG;
                if ((IMG_Init(img_flags) & img_flags) != img_flags) {
                    printf("Failed to initialize SDL2_image: %s\n",
                        IMG_GetError());
                    success= false;
                }
            }
        }
    }

    return success;
}

bool load_media()
{
    bool success = true;

    return success;
}

void destroy()
{
    if (g_renderer != NULL) {
        SDL_DestroyRenderer(g_renderer);
        g_renderer = NULL;
    }

    if (g_window != NULL) {
        SDL_DestroyWindow(g_window);
        g_window = NULL;
    }

    IMG_Quit();
    SDL_Quit();
}
