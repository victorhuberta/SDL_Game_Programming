#include <SDL2/SDL.h>
#include "ParticleEmitter.h"

bool ParticleEmitter::init()
{
    bool success = true;

    glow_particle.set_renderer(renderer);
    success = glow_particle.load_from_file(GLOW_PARTICLE_PATH);

    for (int i = 0; i < N_PARTICLES; ++i) {
        particles[i].set_renderer(renderer);
        int j = rand() % N_COLOR_PARTICLES;
        success = particles[i].load_from_file(COLOR_PARTICLE_PATHS[j]) &&
            success;
    }

    return success;
}

void ParticleEmitter::update()
{
    for (int i = 0; i < N_PARTICLES; ++i) {
        int randx = entity.get_x() - 5 + (rand() % 25);
        int randy = entity.get_y() - 5 + (rand() % 25);

        particles[i].set_xy(randx, randy);
    }
}

void ParticleEmitter::render()
{
    entity.render();

    for (int i = 0; i < N_PARTICLES; ++i) {
        if (particles[i].is_dead()) {
            glow_particle.revive();
            glow_particle.set_xy(particles[i].get_x(), particles[i].get_y());
            glow_particle.render();

            int j = rand() % N_COLOR_PARTICLES;
            particles[i].load_from_file(COLOR_PARTICLE_PATHS[j]);
            particles[i].revive();
        } else {
            particles[i].render();
        }
    }
}

void ParticleEmitter::destroy()
{
    entity.destroy();

    for (int i = 0; i < N_PARTICLES; ++i) {
        particles[i].destroy();
    }

    glow_particle.destroy();
}
