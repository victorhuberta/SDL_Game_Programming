#include <SDL2/SDL.h>
#include <string>
#include "Particle.h"

Particle::Particle()
{
    life = rand() % N_MAX_LIVES;
    x = 0;
    y = 0;
    dead = false;
}

bool Particle::load_from_file(const std::string path)
{
    return tex.load_from_file(path);
}

void Particle::render()
{
    if (dead) {
        printf("Warning: particle is dead, do NOT render\n");
        return;
    }

    --life;
    if (life <= 0) {
        dead = true;
    }
    tex.render(x, y);
}
