#include <SDL2/SDL.h>
#include <string>
#include "Dot.h"

Dot::Dot(SDL_Renderer* r, std::string path)
{
    dot_tex.set_renderer(r);
    if (! dot_tex.load_from_file(path)) {
        throw FailedToCreateDot {"Invalid path given"};
    }

    x = 0;
    y = 0;
    vel_x = 0;
    vel_y = 0;
}

void Dot::handle_event(SDL_Event& e)
{
    if ((e.type == SDL_KEYDOWN) && (e.key.repeat == 0)) {
        switch (e.key.keysym.sym) {
            case SDLK_UP: vel_y -= DOT_VEL; break;
            case SDLK_DOWN: vel_y += DOT_VEL; break;
            case SDLK_LEFT: vel_x -= DOT_VEL; break;
            case SDLK_RIGHT: vel_x += DOT_VEL; break;
        }
    } else if ((e.type == SDL_KEYUP) && (e.key.repeat == 0)) {
        switch (e.key.keysym.sym) {
            case SDLK_UP: vel_y += DOT_VEL; break;
            case SDLK_DOWN: vel_y -= DOT_VEL; break;
            case SDLK_LEFT: vel_x += DOT_VEL; break;
            case SDLK_RIGHT: vel_x -= DOT_VEL; break;
        }
    }
}

void Dot::move(int max_x, int max_y)
{
    x += vel_x;

    if ((x < 0) || ((x + DOT_WIDTH) > max_x)) {
        x -= vel_x;
    }

    y += vel_y;

    if ((y < 0) || ((y + DOT_HEIGHT) > max_y)) {
        y -= vel_y;
    }
}

void Dot::render()
{
    dot_tex.render(x, y);
}
