#ifndef _DOT_H
#define _DOT_H

#include <SDL2/SDL.h>
#include <exception>
#include <string>
#include "LTexture.h"

class Dot
{
    public:
        Dot(SDL_Renderer* r, std::string path);
        ~Dot() { dot_tex.free(); }

        static const int DOT_WIDTH = 20;
        static const int DOT_HEIGHT = 20;
        static const int DOT_VEL = 10;

        void handle_event(SDL_Event* e);
        void move(int max_x = -1, int max_y = -1, SDL_Rect* obj_rect = NULL);
        void render();
        bool in_collision(SDL_Rect* obj_rect);

    private:
        LTexture dot_tex;
        SDL_Rect dot_rect;

        int x, y;
        int vel_x, vel_y;
};

class FailedToCreateDot: public std::exception
{
    public:
        FailedToCreateDot(std::string m) : err_msg {m} {}

        virtual const char* what() const noexcept
        {
            return err_msg.c_str();
        }

    private:
        std::string err_msg;
};

#endif
