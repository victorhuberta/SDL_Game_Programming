#include <SDL2/SDL.h>
#include <string>
#include "Dot.h"

Dot::Dot(SDL_Renderer* r, std::string path)
{
    dot_tex.set_renderer(r);
    if (! dot_tex.load_from_file(path)) {
        throw FailedToCreateDot {"Invalid path given"};
    }

    x = 0;
    y = 0;
    vel_x = 0;
    vel_y = 0;

    dot_rect.x = x;
    dot_rect.y = y;
    dot_rect.w = DOT_WIDTH;
    dot_rect.h = DOT_HEIGHT;
}
    

void Dot::handle_event(SDL_Event* e)
{
    if ((e->type == SDL_KEYDOWN) && (e->key.repeat == 0)) {
        switch (e->key.keysym.sym) {
            case SDLK_UP: vel_y -= DOT_VEL; break;
            case SDLK_DOWN: vel_y += DOT_VEL; break;
            case SDLK_LEFT: vel_x -= DOT_VEL; break;
            case SDLK_RIGHT: vel_x += DOT_VEL; break;
        }
    } else if ((e->type == SDL_KEYUP) && (e->key.repeat == 0)) {
        switch (e->key.keysym.sym) {
            case SDLK_UP: vel_y += DOT_VEL; break;
            case SDLK_DOWN: vel_y -= DOT_VEL; break;
            case SDLK_LEFT: vel_x += DOT_VEL; break;
            case SDLK_RIGHT: vel_x -= DOT_VEL; break;
        }
    }
}

void Dot::move(int max_x, int max_y, SDL_Rect* obj_rect)
{
    x += vel_x;
    dot_rect.x += vel_x;

    if ((x < 0) || ((x + DOT_WIDTH) > max_x) || in_collision(obj_rect)) {
        x -= vel_x;
        dot_rect.x -= vel_x;
    }

    y += vel_y;
    dot_rect.y += vel_y;

    if ((y < 0) || ((y + DOT_HEIGHT) > max_y) || in_collision(obj_rect)) {
        y -= vel_y;
        dot_rect.y -= vel_y;
    }
}

void Dot::render()
{
    dot_tex.render(x, y);
}

bool Dot::in_collision(SDL_Rect* obj_rect)
{
    int top_A = dot_rect.y, bottom_A = dot_rect.y + dot_rect.h;
    int left_A = dot_rect.x, right_A = dot_rect.x + dot_rect.w;

    int top_B = obj_rect->y, bottom_B = obj_rect->y + obj_rect->h;
    int left_B = obj_rect->x, right_B = obj_rect->x + obj_rect->w;

    return ! ((top_A >= bottom_B) || (bottom_A <= top_B) ||
        (right_A <= left_B) || (left_A >= right_B));
}
