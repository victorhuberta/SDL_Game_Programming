#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>
#include <stdio.h>

bool init();
SDL_Surface *loadSurface(std::string path);
void destroy();

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

SDL_Window *gWindow = NULL;
SDL_Surface *gMainSurface = NULL;

int main(int argc, char **argv)
{
    if (! init()) {
        printf("Failed to initialize!\n");
        return 1;
    }

    SDL_Surface *gSurface = loadSurface("cat.png");
    if (gSurface == NULL) {
        printf("Failed to load image surface!\n");
        SDL_DestroyWindow(gWindow);
        return 1;
    }

    bool quit = false;
    SDL_Event e;

    SDL_Rect stretchRect = {};
    stretchRect.x = 0;
    stretchRect.y = 0;
    stretchRect.w = SCREEN_WIDTH;
    stretchRect.h = SCREEN_HEIGHT;

    while (! quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            }
        }

        SDL_BlitScaled(gSurface, NULL, gMainSurface, &stretchRect);
        SDL_UpdateWindowSurface(gWindow);
    }

    SDL_FreeSurface(gSurface);
    destroy();
    return 0;
}

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Failed to initialize SDL: %s\n", SDL_GetError());
        success = false;
    } else {

        gWindow = SDL_CreateWindow("STRETCH", SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT,
            SDL_WINDOW_SHOWN);

        if (gWindow == NULL) {
            printf("Failed to create window: %s\n", SDL_GetError());
            success = false;
        } else {

            int imgFlags = IMG_INIT_PNG;
            if ((IMG_Init(imgFlags) & imgFlags) != imgFlags) {
                printf("Failed to initialize SDL_image: %s\n", IMG_GetError());
                success = false;
            } else {
                gMainSurface = SDL_GetWindowSurface(gWindow);
            }
        }
    }

    return success;
}

SDL_Surface *loadSurface(std::string path)
{
    SDL_Surface *optimizedSurface = NULL;

    SDL_Surface *imgSurface = IMG_Load(path.c_str());
    if (imgSurface == NULL) {
        printf("Failed to load image '%s': %s\n", path.c_str(), IMG_GetError());
    } else {
        optimizedSurface = SDL_ConvertSurface(imgSurface,
            gMainSurface->format, 0);

        if (optimizedSurface == NULL) {
            printf("Failed to convert surface '%s': %s\n", path.c_str(),
                SDL_GetError());
        }

        SDL_FreeSurface(imgSurface);
    }

    return optimizedSurface;
}

void destroy()
{
    SDL_DestroyWindow(gWindow);
    gWindow = NULL;
    gMainSurface = NULL;

    IMG_Quit();
    SDL_Quit();
}
