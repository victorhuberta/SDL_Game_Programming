#include <SDL2/SDL.h>
#include <vector>
#include "VWindow.h"

bool init();
void destroy();

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int N_WINDOWS = 3;

std::vector<VWindow> g_windows;

int main(int argc, char* argv[])
{
    if (! init()) {
        printf("Failed to initialize!\n");
        return 1;
    }

    for (int i = 0; i < N_WINDOWS; ++i) {
        VWindow g_window {"WINDOW EVENTS", SCREEN_WIDTH, SCREEN_HEIGHT};
        if (! g_window.init()) {
            printf("Failed to initialize VWindow: %s\n", SDL_GetError());
            destroy();
            return 1;
        }
        g_windows.push_back(g_window);
    }

    bool quit = false;
    SDL_Event e;
    SDL_Color color = {0xFF, 0xFF, 0, 0xFF};

    while (! quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            }

            for (auto g_window : g_windows) {
                g_window.handle_event(e);
            }

            if (e.type == SDL_KEYDOWN) {
                switch (e.key.keysym.sym) {
                    case SDLK_1:
                        g_windows[0].focus();
                        break;
                    case SDLK_2:
                        g_windows[1].focus();
                        break;
                    case SDLK_3:
                        g_windows[2].focus();
                        break;
                }
            }
        }

        for (auto g_window : g_windows) {
            if (! g_window.is_minimized()) {
                int w = 300;
                int h = 400;
                g_window.draw_rect((SCREEN_WIDTH - w) / 2,
                    (SCREEN_HEIGHT - h) / 2, w, h, color);
            }
        }

        quit = true;
        for (auto g_window : g_windows) {
            if (! g_window.is_closed()) {
                quit = false;
                break;
            }
        }
    }

    destroy();
    return 0;
}

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Failed to initialize SDL2: %s\n", SDL_GetError());
        success = false;
    }

    return success;
}

void destroy()
{
    for (auto g_window : g_windows) {
        g_window.destroy();
    }
    SDL_Quit();
}
