#include <SDL2/SDL.h>
#include <string>
#include <iostream>
#include "VWindow.h"

VWindow::VWindow(std::string t, int w, int h)
{
    window_id = -1;
    window = NULL;
    renderer = NULL;
    title = t;
    width = w;
    height = h;

    shown = false;
    closed = false;
    exposed = false;
    minimized = false;
    maximized = false;
    fullscreen = false;
    mouse_focused = false;
    keyboard_focused = false;
}

bool VWindow::init()
{
    if (window != NULL) return renderer != NULL;

    window = SDL_CreateWindow(title.c_str(),
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        width, height, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (window != NULL) {
        shown = true;
        exposed = true;
        mouse_focused = true;
        keyboard_focused = true;
        window_id = SDL_GetWindowID(window);

        renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED |
            SDL_RENDERER_PRESENTVSYNC);
        if (renderer == NULL) {
            printf("Failed to create renderer: %s\n", SDL_GetError());
        }
    }

    return ( (window != NULL) && (renderer != NULL) );
}

void VWindow::clear()
{
    SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
    SDL_RenderClear(renderer);
}

void VWindow::repaint()
{
    SDL_RenderPresent(renderer);
}

void VWindow::draw_rect(int x, int y, int w, int h, SDL_Color& c)
{
    clear();

    SDL_Rect rect = {x, y, w, h};
    SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, c.a);
    SDL_RenderFillRect(renderer, &rect);

    repaint();
}

void VWindow::handle_event(SDL_Event& e)
{
    if (e.type == SDL_WINDOWEVENT && e.window.windowID == window_id) {
        switch (e.window.event) {
            case SDL_WINDOWEVENT_SIZE_CHANGED:
                width = e.window.data1;
                height = e.window.data2;
                break;
            case SDL_WINDOWEVENT_SHOWN:
                shown = true;
                closed = false;
                break;
            case SDL_WINDOWEVENT_EXPOSED:
                exposed = true;
                break;
            case SDL_WINDOWEVENT_HIDDEN:
                shown = false;
                exposed = false;
                break;
            case SDL_WINDOWEVENT_MINIMIZED:
                minimized = true;
                break;
            case SDL_WINDOWEVENT_MAXIMIZED:
                maximized = true;
                break;
            case SDL_WINDOWEVENT_RESTORED:
                shown = true;
                exposed = true;
                minimized = false;
                maximized = false;
                break;
            case SDL_WINDOWEVENT_ENTER:
                mouse_focused = true;
                break;
            case SDL_WINDOWEVENT_LEAVE:
                mouse_focused = false;
                break;
            case SDL_WINDOWEVENT_FOCUS_GAINED:
                keyboard_focused = true;
                break;
            case SDL_WINDOWEVENT_FOCUS_LOST:
                keyboard_focused = false;
                break;
            case SDL_WINDOWEVENT_CLOSE:
                closed = true;
                SDL_HideWindow(window);
                break;
        }

        print_state();
    } else if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_RETURN) {
        if (! fullscreen) {
            SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN);
            fullscreen = true;
        } else {
            SDL_SetWindowFullscreen(window, 0);
            fullscreen = false;
        }
    }
}

void VWindow::print_state()
{
    std::cout << std::endl;
    std::cout << "Window ID: " << window_id << std::endl;
    std::cout << "Title: " << title << std::endl;
    std::cout << "Width: " << width << "; Height: " << height << std::endl;
    std::cout << "Is Shown? " << shown << std::endl;
    std::cout << "Is Closed? " << closed << std::endl;
    std::cout << "Is Exposed? " << exposed << std::endl;
    std::cout << "Is Minimized? " << minimized << std::endl;
    std::cout << "Is Maximized? " << maximized << std::endl;
    std::cout << "Is Fullscreen? " << fullscreen << std::endl;
    std::cout << "Has Mouse Focus? " << mouse_focused << std::endl;
    std::cout << "Has Keyboard Focus? " << keyboard_focused << std::endl;
    std::cout << std::endl;
}

void VWindow::focus()
{
    if (! shown) {
        SDL_ShowWindow(window);
        shown = true;
    }

    SDL_RaiseWindow(window);
}

void VWindow::destroy()
{
    if (renderer != NULL) {
        SDL_DestroyRenderer(renderer);
        renderer = NULL;
    }

    if (window != NULL) {
        SDL_DestroyWindow(window);
        window = NULL;
    }
}
