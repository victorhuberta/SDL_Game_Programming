#include <SDL2/SDL.h>
#include <string>

class VWindow
{
    public:
        VWindow(std::string t, int w, int h);
        ~VWindow() { destroy(); }

        bool init();
        void clear();
        void repaint();
        void draw_rect(int x, int y, int w, int h, SDL_Color& c);
        void handle_event(SDL_Event& e);
        void print_state();
        void focus();

        int get_width() { return width; }
        int get_height() { return height; }
        bool is_closed() { return closed; }
        bool is_exposed() { return exposed; }
        bool is_minimized() { return minimized; }
        bool is_maximized() { return maximized; }
        bool is_fullscreen() { return fullscreen; }
        bool is_shown() { return shown; }
        bool has_mouse_focus() { return mouse_focused; }
        bool has_keyboard_focus() { return keyboard_focused; }

        void destroy();

    private:
        SDL_Window* window;
        SDL_Renderer* renderer;

        Uint32 window_id;
        std::string title;
        int width, height;
        bool closed, exposed, minimized, maximized, fullscreen;
        bool shown, mouse_focused, keyboard_focused;
};
