#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include <stdio.h>

#include "LTexture.h"
#include "VTimer.h"

bool init();
bool load_media();
void destroy();

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int FRAMES_PER_SECOND = 60;
const double MIN_TICKS_PER_FRAME = 1000.0 / FRAMES_PER_SECOND;

const char* FONT_PATH = "font.ttf";
const int FONT_SIZE = 20;
const std::string TEXT_STR = "Average FPS: ";

SDL_Window* g_window = NULL;
SDL_Renderer* g_renderer = NULL;
TTF_Font* g_font = NULL;
LTexture text;

int main(int argc, char* argv[])
{
    if (! init()) {
        printf("Failed to initialize!\n");
        return 1;
    }

    if (! load_media()) {
        printf("Failed to load media!\n");
        destroy();
        return 1;
    }

    bool quit = false;
    SDL_Event e;

    SDL_Color color = {0, 0, 0, 0xFF};

    VTimer f_timer;
    VTimer timer;
    timer.start();

    int n_frames = 0;

    while (! quit) {
        f_timer.start();

        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            }
        }

        SDL_SetRenderDrawColor(g_renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear(g_renderer);

        text.load_text_from_font(g_font, TEXT_STR +
            std::to_string(n_frames / timer.to_seconds()), color);

        int x = (SCREEN_WIDTH - text.get_width()) / 2;
        int y = (SCREEN_HEIGHT - text.get_height()) / 2;
        text.render(x, y);

        SDL_RenderPresent(g_renderer);
        ++n_frames;

        Uint32 frame_ticks = f_timer.get_ticks();
        if (frame_ticks < MIN_TICKS_PER_FRAME) {
            SDL_Delay(MIN_TICKS_PER_FRAME - frame_ticks);
        }

        f_timer.stop();
    }

    timer.stop();
    destroy();
    return 0;
}

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Failed to initialize SDL: %s\n", SDL_GetError());
        success = false;
    } else {
        
        g_window = SDL_CreateWindow("FONT DEMO", SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT,
            SDL_WINDOW_SHOWN);
        if (g_window == NULL) {
            printf("Failed to create window: %s\n", SDL_GetError());
            success = false;
        } else {

            g_renderer = SDL_CreateRenderer(g_window, -1,
                SDL_RENDERER_ACCELERATED);
            if (g_renderer == NULL) {
                printf("Failed to create renderer: %s\n", SDL_GetError());
                success = false;
            } else {

                if (TTF_Init() == -1) {
                    printf("Failed to initialize SDL_ttf: %s\n", TTF_GetError());
                    success = false;
                }
            }
        }
    }

    return success;
}

bool load_media()
{
    bool success = true;

    g_font = TTF_OpenFont(FONT_PATH, FONT_SIZE);
    if (g_font == NULL) {
        printf("Failed to open font: %s\n", TTF_GetError());
        success = false;
    } else {
        text.set_renderer(g_renderer);
    }

    return success;
}

void destroy()
{
    text.free();

    if (g_font != NULL) {
        TTF_CloseFont(g_font);
        g_font = NULL;
    }

    if (g_renderer != NULL) {
        SDL_DestroyRenderer(g_renderer);
        g_renderer = NULL;
    }

    if (g_window != NULL) {
        SDL_DestroyWindow(g_window);
        g_window = NULL;
    }

    TTF_Quit();
    SDL_Quit();
}
