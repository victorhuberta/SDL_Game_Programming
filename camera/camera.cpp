#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>

#include "LTexture.h"
#include "Dot.h"

bool init();
bool load_media();
void destroy();

const int LEVEL_WIDTH = 1280;
const int LEVEL_HEIGHT = 960;
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

SDL_Window* g_window = NULL;
SDL_Renderer* g_renderer = NULL;
LTexture bg_tex;

int main(int argc, char* argv[])
{
    if (! init()) {
        printf("Failed to initialize!\n");
        return 1;
    }

    if (! load_media()) {
        printf("Failed to load media!\n");
        destroy();
        return 1;
    }

    bool quit = false;
    SDL_Event e;
    Dot dot1 {g_renderer, "dot.bmp", 0, 0};
    Dot dot2 {g_renderer, "dot.bmp", 50, 80};
    SDL_Rect wall = {300, 40, 40, 400};
    SDL_Rect camera = {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT};

    while (! quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            }

            dot1.handle_event(&e);
        }

        dot1.move(LEVEL_WIDTH, LEVEL_HEIGHT, &wall, dot2);

        camera.x = dot1.get_x() - camera.w/2;
        camera.y = dot1.get_y() - camera.h/2;
        int max_cam_x = LEVEL_WIDTH - camera.w;
        int max_cam_y = LEVEL_HEIGHT - camera.h;

        camera.x = (camera.x < 0) ? 0 :
            ( (camera.x > max_cam_x) ? max_cam_x : camera.x );
        camera.y = (camera.y < 0) ? 0 :
            ( (camera.y > max_cam_y) ? max_cam_y : camera.y );

        SDL_SetRenderDrawColor(g_renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear(g_renderer);

        bg_tex.render(0, 0, &camera);

        SDL_SetRenderDrawColor(g_renderer, 0xFF, 0, 0, 0xFF);
        SDL_RenderDrawRect(g_renderer, &wall);

        dot1.render(camera.x, camera.y);
        dot2.render();

        SDL_RenderPresent(g_renderer);
    }

    destroy();
    return 0;
}

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Failed to initialize SDL: %s\n", SDL_GetError());
        success = false;
    } else {
        
        g_window = SDL_CreateWindow("CAMERA DEMO", SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT,
            SDL_WINDOW_SHOWN);
        if (g_window == NULL) {
            printf("Failed to create window: %s\n", SDL_GetError());
            success = false;
        } else {

            g_renderer = SDL_CreateRenderer(g_window, -1,
                SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
            if (g_renderer == NULL) {
                printf("Failed to create renderer: %s\n", SDL_GetError());
                success = false;
            } else {

                int img_flags = IMG_INIT_PNG;
                if ( (IMG_Init(img_flags) & img_flags) != img_flags ) {
                    printf("Failed to initialize SDL_image: %s\n",
                        IMG_GetError());
                    success = false;
                }
            }
        }
    }

    return success;
}

bool load_media()
{
    bool success = true;

    bg_tex.set_renderer(g_renderer);
    success = bg_tex.load_from_file("bg.png");

    return success;
}

void destroy()
{
    bg_tex.free();

    if (g_renderer != NULL) {
        SDL_DestroyRenderer(g_renderer);
        g_renderer = NULL;
    }

    if (g_window != NULL) {
        SDL_DestroyWindow(g_window);
        g_window = NULL;
    }

    IMG_Quit();
    SDL_Quit();
}
