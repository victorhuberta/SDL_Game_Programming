#ifndef LTEXTURE_H
#define LTEXTURE_H

#include <SDL2/SDL.h>
#include <string>

class LTexture
{
    public:
        LTexture() : texture {NULL}, renderer {NULL}, width {0}, height {0} {}
        ~LTexture() { free(); }

        void set_renderer(SDL_Renderer* r) { renderer = r; }

#ifdef _SDL_TTF_H
        bool load_text_from_font(TTF_Font* font,
            std::string text_str, SDL_Color color);
#endif

        bool load_from_file(std::string path);
        void render(int x, int y, SDL_Rect* clip = NULL);
        void free();
        int get_width() { return width; }
        int get_height() { return height; }

    private:
        SDL_Texture* texture;
        SDL_Renderer* renderer;
        int width, height;
};

#endif
