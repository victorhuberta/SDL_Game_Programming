#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <exception>
#include <string>
#include <stdio.h>

const char* WINDOW_TITLE = "ALPHA BLENDING";
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

class NoRenderer: public std::exception
{
    public:
        const char* what() const noexcept { return "No renderer specified."; }
} no_renderer;

class LTexture
{
    public:
        LTexture() : texture {NULL}, width {0}, height {0} {} 
        ~LTexture() { free(); }

        bool load_from_file(std::string path);
        void set_renderer(SDL_Renderer* r) { renderer = r; }
        void set_color(Uint8 r, Uint8 g, Uint8 b);
        void set_blendmode(SDL_BlendMode mode);
        void set_alpha(Uint8 a);
        void render(int x, int y, SDL_Rect* clip = NULL, bool stretch = false);
        void free();

        int get_width() const { return width; }
        int get_height() const { return height; } 

    private:
        SDL_Texture* texture;
        SDL_Renderer* renderer;
        int width, height;
};

bool LTexture::load_from_file(std::string path)
{
    if (renderer == NULL) {
        throw no_renderer;
    }

    free();

    SDL_Texture* loaded_texture = NULL;
    SDL_Surface* loaded_surface = IMG_Load(path.c_str());
    if (loaded_surface == NULL) {
        printf("Failed to load image '%s': %s\n", path.c_str(), IMG_GetError());
    } else {

        SDL_SetColorKey(loaded_surface, SDL_TRUE,
            SDL_MapRGB(loaded_surface->format, 0x00, 0xFF, 0xFF));

        loaded_texture = SDL_CreateTextureFromSurface(renderer, loaded_surface);
        if (loaded_texture == NULL) {
            printf("Failed to create texture: %s\n", SDL_GetError());
        } else {
            width = loaded_surface->w;
            height = loaded_surface->h;
        }

        SDL_FreeSurface(loaded_surface);
    }

    texture = loaded_texture;
    return texture != NULL;
}

void LTexture::set_color(Uint8 r, Uint8 g, Uint8 b)
{
    if (SDL_SetTextureColorMod(texture, r, g, b) < 0) {
        printf("Failed to modulate color: %s\n", SDL_GetError());
    }
}

void LTexture::set_blendmode(SDL_BlendMode mode)
{
    if (SDL_SetTextureBlendMode(texture, mode) < 0) {
        printf("Failed to set blend mode: %s\n", SDL_GetError());
    }
}

void LTexture::set_alpha(Uint8 a)
{
    if (SDL_SetTextureAlphaMod(texture, a) < 0) {
        printf("Failed to modulate alpha: %s\n", SDL_GetError());
    }
}

void LTexture::render(int x, int y, SDL_Rect* clip, bool stretch)
{
    if (renderer == NULL) {
        throw no_renderer;
    }

    SDL_Rect renderQuad {x, y, width, height};

    if (clip != NULL) {
        renderQuad.w = clip->w;
        renderQuad.h = clip->h;
    }

    if (stretch) {
        renderQuad.w = SCREEN_WIDTH;
        renderQuad.h = SCREEN_HEIGHT;
    }

    SDL_RenderCopy(renderer, texture, clip, &renderQuad);
}

void LTexture::free()
{
    if (texture != NULL) {
        SDL_DestroyTexture(texture);
        texture = NULL;
        width = 0;
        height = 0;
    }
}

bool init();
bool load_media(LTexture& tex, std::string path);
void destroy();

SDL_Window* g_window = NULL;
SDL_Renderer* g_renderer = NULL;

const std::string BACK_PATH = "back.png";
const std::string FRONT_PATH = "front.png";
LTexture back;
LTexture front;

int main(int argc, char* argv[])
{
    if (! init()) {
        printf("Failed to initialize!\n");
        destroy();
        return 1;
    }

    back.set_renderer(g_renderer);
    front.set_renderer(g_renderer);

    if (! load_media(back, BACK_PATH) || ! load_media(front, FRONT_PATH)) {
        destroy();
        return 1;
    }

    back.set_blendmode(SDL_BLENDMODE_BLEND);
    front.set_blendmode(SDL_BLENDMODE_BLEND);

    bool quit = false;
    SDL_Event e;
    Uint8 a = 0xFF;
    Uint8 mod_delta = 0x20;

    while (! quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            } else if (e.type == SDL_KEYDOWN) {
                switch (e.key.keysym.sym) {
                    case SDLK_s:
                        if (a - mod_delta < 0) {
                            a = 0;
                        } else {
                            a -= mod_delta;
                        }
                        break;
                    case SDLK_w:
                        if (a + mod_delta > 0xFF) {
                            a = 0xFF;
                        } else {
                            a += mod_delta;
                        }
                        break;
                }
            }
        }

        SDL_SetRenderDrawColor(g_renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear(g_renderer);

        back.render(0, 0, NULL, true);

        front.set_alpha(a);
        front.render(0, 0, NULL, true);

        SDL_RenderPresent(g_renderer);
    }

    return 0;
}

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Failed to initialize SDL: %s\n", SDL_GetError());
        success = false;
    } else {
        g_window = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT,
            SDL_WINDOW_SHOWN);
        if (g_window == NULL) {
            printf("Failed to create window: %s\n", SDL_GetError());
            success = false;
        } else {
            g_renderer = SDL_CreateRenderer(g_window, -1, SDL_RENDERER_ACCELERATED);
            if (g_renderer == NULL) {
                printf("Failed to create renderer: %s\n", SDL_GetError());
                success = false;
            }
        }
    }

    if (IMG_Init(IMG_INIT_PNG) < 0) {
        printf("Failed to initialize SDL_image: %s\n", IMG_GetError());
        success = false;
    }

    return success;
}

bool load_media(LTexture& tex, std::string path)
{
    try {
        if (! tex.load_from_file(path)) {
            printf("Failed to load from file!\n");
            return false;
        }
    } catch (NoRenderer& e) {
        printf("NoRenderer: %s\n", e.what());
        return false;
    }

    return true;
}

void destroy()
{
    back.free();
    front.free();

    if (g_renderer != NULL) {
        SDL_DestroyRenderer(g_renderer);
        g_renderer = NULL;
    }

    if (g_window != NULL) {
        SDL_DestroyWindow(g_window);
        g_window = NULL;
    }

    IMG_Quit();
    SDL_Quit();
}
