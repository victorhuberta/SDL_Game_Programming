#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <stdio.h>
#include <string>

bool init();
bool load_media();
void destroy();

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

const int N_SAMPLES = 4;
const std::string SAMPLE_NAMES[N_SAMPLES] = {
    "scratch",
    "high",
    "medium",
    "low"
};

SDL_Window* g_window = NULL;
SDL_Renderer* g_renderer = NULL;
SDL_Texture* g_texture = NULL;

Mix_Music* music = NULL;
Mix_Chunk* samples[N_SAMPLES] = {};

int main(int argc, char* argv[])
{
    if (! init()) {
        printf("Failed to initialize!\n");
        return 1;
    }

    if (! load_media()) {
        printf("Failed to load media!\n");
        destroy();
        return 1;
    }

    bool quit = false;
    SDL_Event e;

    while (! quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            } else if (e.type == SDL_KEYDOWN) {
                switch (e.key.keysym.sym) {
                    case SDLK_1:
                        if (Mix_PlayChannel(-1, samples[0], 0) == -1) {
                            printf("Failed to play sample: %s\n",
                                Mix_GetError());
                        }
                        break;
                    case SDLK_2:
                        if (Mix_PlayChannel(-1, samples[1], 0) == -1) {
                            printf("Failed to play sample: %s\n",
                                Mix_GetError());
                        }
                        break;
                    case SDLK_3:
                        if (Mix_PlayChannel(-1, samples[2], 0) == -1) {
                            printf("Failed to play sample: %s\n",
                                Mix_GetError());
                        }
                        break;
                    case SDLK_4:
                        if (Mix_PlayChannel(-1, samples[3], 0) == -1) {
                            printf("Failed to play sample: %s\n",
                                Mix_GetError());
                        }
                        break;
                    case SDLK_9:
                        if (Mix_PlayingMusic() == 0) {
                            if (Mix_PlayMusic(music, -1) == -1) {
                                printf("Failed to play music\n");
                            }
                        } else {

                            if (Mix_PausedMusic() == 1) {
                                Mix_ResumeMusic();
                            } else {
                                Mix_PauseMusic();
                            }
                        }
                        break;
                    case SDLK_0:
                        Mix_FadeOutMusic(1500);
                        break;
                }
            }
        }

        SDL_SetRenderDrawColor(g_renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear(g_renderer);

        SDL_RenderCopy(g_renderer, g_texture, NULL, NULL);

        SDL_RenderPresent(g_renderer);
    }

    destroy();
    return 0;
}

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
        printf("Failed to initialize SDL: %s\n", SDL_GetError());
        success = false;
    } else {
        
        g_window = SDL_CreateWindow("FONT DEMO", SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT,
            SDL_WINDOW_SHOWN);
        if (g_window == NULL) {
            printf("Failed to create window: %s\n", SDL_GetError());
            success = false;
        } else {

            g_renderer = SDL_CreateRenderer(g_window, -1,
                SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
            if (g_renderer == NULL) {
                printf("Failed to create renderer: %s\n", SDL_GetError());
                success = false;
            }

            int img_flags = IMG_INIT_PNG;
            if ((IMG_Init(img_flags) & img_flags) != img_flags) {
                printf("Failed to initialize SDL_image: %s\n", IMG_GetError());
                success = false;
            }

            if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) == -1) {
                printf("Failed to initialize SDL_mixer: %s\n", Mix_GetError());
                success = false;
            }
        }
    }

    return success;
}

bool load_media()
{
    bool success = true;

    SDL_Surface* loaded_surface = IMG_Load("prompt.png");
    if (loaded_surface == NULL) {
        printf("Failed to load image: %s\n", IMG_GetError());
        success = false;
    } else {

        g_texture = SDL_CreateTextureFromSurface(g_renderer, loaded_surface);
        if (g_texture == NULL) {
            printf("Failed to create texture: %s\n", SDL_GetError());
            success = false;
        }

        SDL_FreeSurface(loaded_surface);
    }

    music = Mix_LoadMUS("21_sound_effects_and_music/beat.wav");
    if (music == NULL) {
        printf("Failed to load beat music: %s\n", Mix_GetError());
        success = false;
    }

    for (int i = 0; i < N_SAMPLES; ++i) {
        std::string sample_name = "21_sound_effects_and_music/" +
            SAMPLE_NAMES[i] + ".wav";
        samples[i] = Mix_LoadWAV(sample_name.c_str());
        if (samples[i] == NULL) {
            printf("Failed to load sound sample: %s\n", Mix_GetError());
            success = false;
        }
    }

    return success;
}

void destroy()
{
    if (music != NULL) {
        Mix_FreeMusic(music);
        music = NULL;
    }

    for (int i = 0; i < N_SAMPLES; ++i) {
        if (samples[i] != NULL) {
            Mix_FreeChunk(samples[i]);
            samples[i] = NULL;
        }
    }

    Mix_CloseAudio();

    if (g_texture != NULL) {
        SDL_DestroyTexture(g_texture);
        g_texture = NULL;
    }

    if (g_renderer != NULL) {
        SDL_DestroyRenderer(g_renderer);
        g_renderer = NULL;
    }

    if (g_window != NULL) {
        SDL_DestroyWindow(g_window);
        g_window = NULL;
    }

    SDL_Quit();
}
