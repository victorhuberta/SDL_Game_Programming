#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <exception>
#include <string>
#include <stdio.h>

class NoRenderer: public std::exception
{
    public:
        const char* what() const noexcept { return "No renderer specified."; }
} no_renderer;

class LTexture
{
    public:
        LTexture() : texture {NULL}, width {0}, height {0} {} 
        ~LTexture() { free(); }

        bool load_from_file(std::string path);
        void set_renderer(SDL_Renderer* r) { renderer = r; }
        void render(int x, int y);
        void free();

        int get_width() const { return width; }
        int get_height() const { return height; } 

    private:
        SDL_Texture* texture;
        SDL_Renderer* renderer;
        int width, height;
};

bool LTexture::load_from_file(std::string path)
{
    if (renderer == NULL) {
        throw no_renderer;
    }

    free();

    SDL_Texture* loaded_texture = NULL;
    SDL_Surface* loaded_surface = IMG_Load(path.c_str());
    if (loaded_surface == NULL) {
        printf("Failed to load image '%s': %s\n", path.c_str(), IMG_GetError());
    } else {

        SDL_SetColorKey(loaded_surface, SDL_TRUE,
            SDL_MapRGB(loaded_surface->format, 0x00, 0xFF, 0xFF));

        loaded_texture = SDL_CreateTextureFromSurface(renderer, loaded_surface);
        if (loaded_texture == NULL) {
            printf("Failed to create texture: %s\n", SDL_GetError());
        } else {
            width = loaded_surface->w;
            height = loaded_surface->h;
        }

        SDL_FreeSurface(loaded_surface);
    }

    texture = loaded_texture;
    return texture != NULL;
}

void LTexture::render(int x, int y)
{
    if (renderer == NULL) {
        throw no_renderer;
    }

    SDL_Rect renderQuad {x, y, width, height};
    SDL_RenderCopy(renderer, texture, NULL, &renderQuad);
}

void LTexture::free()
{
    if (texture != NULL) {
        SDL_DestroyTexture(texture);
        texture = NULL;
        width = 0;
        height = 0;
    }
}

bool init();
void destroy();

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const char* STICKMAN_PATH = "stickman.png";
const char* BG_PATH = "bg.png";

SDL_Window* g_window = NULL;
SDL_Renderer* g_renderer = NULL;

LTexture stickman;
LTexture bg;

int main(int argc, char* argv[])
{
    if (! init()) {
        printf("Failed to initialize!\n");
        destroy();
        return 1;
    }

    stickman.set_renderer(g_renderer);
    bg.set_renderer(g_renderer);

    bool success = false;

    try {
        success = stickman.load_from_file(std::string {STICKMAN_PATH});
        if (! success) {
            printf("Failed to load stickman!\n");
            destroy();
            return 1;
        }
    } catch (NoRenderer& e) {
        printf("NoRenderer: %s\n", e.what());
        destroy();
        return 1;
    }

    try {
        success = bg.load_from_file(std::string {BG_PATH});
        if (! success) {
            printf("Failed to load background!\n");
            destroy();
            return 1;
        }
    } catch (NoRenderer& e) {
        printf("NoRenderer: %s\n", e.what());
        destroy();
        return 1;
    }

    bool quit = false;
    SDL_Event e;

    while (! quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            }
        }

        SDL_SetRenderDrawColor(g_renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear(g_renderer);

        bg.render(0, 0);
        stickman.render(240, 190);

        SDL_RenderPresent(g_renderer);
    }

    return 0;
}

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Failed to initialize SDL: %s\n", SDL_GetError());
        success = false;
    } else {
        g_window = SDL_CreateWindow("COLOR KEYYYYING", SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT,
            SDL_WINDOW_SHOWN);
        if (g_window == NULL) {
            printf("Failed to create window: %s\n", SDL_GetError());
            success = false;
        } else {
            g_renderer = SDL_CreateRenderer(g_window, -1, SDL_RENDERER_ACCELERATED);
            if (g_renderer == NULL) {
                printf("Failed to create renderer: %s\n", SDL_GetError());
                success = false;
            }
        }
    }

    if (IMG_Init(IMG_INIT_PNG) < 0) {
        printf("Failed to initialize SDL_image: %s\n", IMG_GetError());
        success = false;
    }

    return success;
}

void destroy()
{
    stickman.free();
    bg.free();

    if (g_renderer != NULL) {
        SDL_DestroyRenderer(g_renderer);
        g_renderer = NULL;
    }

    if (g_window != NULL) {
        SDL_DestroyWindow(g_window);
        g_window = NULL;
    }

    IMG_Quit();
    SDL_Quit();
}
