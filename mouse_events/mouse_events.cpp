#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>

#include "LButton.h"

bool init();
bool load_media();
void destroy();

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int N_BUTTONS = 4;

SDL_Window* g_window = NULL;
SDL_Renderer* g_renderer = NULL;
LButton* buttons[N_BUTTONS] = {};

int main(int argc, char* argv[])
{
    if (! init()) {
        printf("Failed to initialize!\n");
        return 1;
    }

    if (! load_media()) {
        printf("Failed to load media!\n");
        destroy();
        return 1;
    }

    bool quit = false;
    SDL_Event e;

    while (! quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            }

            for (int i = 0; i < N_BUTTONS; ++i) {
                buttons[i]->handle_event(&e);
            }
        }

        SDL_SetRenderDrawColor(g_renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear(g_renderer);

        for (int i = 0; i < N_BUTTONS; ++i) {
            buttons[i]->render();
        }

        SDL_RenderPresent(g_renderer);
    }

    destroy();
    return 0;
}

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Failed to initialize SDL: %s\n", SDL_GetError());
        success = false;
    } else {
        
        g_window = SDL_CreateWindow("FONT DEMO", SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT,
            SDL_WINDOW_SHOWN);
        if (g_window == NULL) {
            printf("Failed to create window: %s\n", SDL_GetError());
            success = false;
        } else {

            g_renderer = SDL_CreateRenderer(g_window, -1,
                SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
            if (g_renderer == NULL) {
                printf("Failed to create renderer: %s\n", SDL_GetError());
                success = false;
            }
        }
    }

    int img_flags = IMG_INIT_PNG;
    if ((IMG_Init(img_flags) & img_flags) != img_flags) {
        printf("Failed to initialize SDL_image: %s\n", IMG_GetError());
        success = false;
    }

    return success;
}

bool load_media()
{
    bool success = true;

    try {
        int x = 0, y = 0;
        for (int i = 0; i < N_BUTTONS; ++i) {
            buttons[i] = new LButton {"button.png", g_renderer};
            buttons[i]->set_position(x, y);

            x += BUTTON_WIDTH;
            if ((x + BUTTON_WIDTH) >= SCREEN_WIDTH) {
                x = 0;
                y += BUTTON_HEIGHT;
            }
        }
    } catch (TextureNotLoaded& e) {
        printf("Failed to load button texture: %s\n", e.what());
        success = false;
    }

    return success;
}

void destroy()
{
    for (int i = 0; i < N_BUTTONS; ++i) {
        if (buttons[i] != NULL) {
            delete buttons[i];
            buttons[i] = NULL;
        }
    }

    if (g_renderer != NULL) {
        SDL_DestroyRenderer(g_renderer);
        g_renderer = NULL;
    }

    if (g_window != NULL) {
        SDL_DestroyWindow(g_window);
        g_window = NULL;
    }
}
