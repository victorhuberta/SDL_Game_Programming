#ifndef LBUTTON_H
#define LBUTTON_H

#include <SDL2/SDL.h>
#include <exception>
#include <string>

#include "LTexture.h"

const int BUTTON_WIDTH = 300;
const int BUTTON_HEIGHT = 200;

enum LButtonSprite
{
    BUTTON_SPRITE_MOUSE_OUT = 0,
    BUTTON_SPRITE_MOUSE_OVER = 1,
    BUTTON_SPRITE_MOUSE_DOWN = 2,
    BUTTON_SPRITE_MOUSE_UP = 3,
    N_BUTTON_SPRITES = 4
};

class LButton
{
    public:
        LButton(std::string path, SDL_Renderer* renderer);
        ~LButton() { free(); }

        void set_position(int x, int y) { position.x = x; position.y = y; }
        void handle_event(SDL_Event* e);
        void render();
        void free();

    private:
        LTexture g_texture;
        SDL_Point position;
        LButtonSprite current_sprite;
        SDL_Rect sprite_clips[N_BUTTON_SPRITES];
};

class TextureNotLoaded: public std::exception
{
    public:
        virtual const char* what() const noexcept
        {
            return "Texture couldn't be loaded with the given file path";
        }
};

#endif
