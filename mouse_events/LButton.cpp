#include <SDL2/SDL.h>
#include <string>

#include "LButton.h"

LButton::LButton(std::string path, SDL_Renderer* renderer)
{
    g_texture.set_renderer(renderer);

    if (! g_texture.load_from_file(path)) {
        throw TextureNotLoaded {};
    }

    position.x = 0;
    position.y = 0;
    current_sprite = BUTTON_SPRITE_MOUSE_OUT;

    int x = 0;
    int y = 0;
    for (int i = 0; i < N_BUTTON_SPRITES; ++i) {
        sprite_clips[i] = SDL_Rect {x, y, BUTTON_WIDTH, BUTTON_HEIGHT};

        x += BUTTON_WIDTH;
        if (x >= g_texture.get_width()) {
            x = 0;
            y += BUTTON_HEIGHT;
            if (y >= g_texture.get_height()) break;
        }
    }
}

void LButton::handle_event(SDL_Event* e)
{
    if (e->type == SDL_MOUSEMOTION || e->type == SDL_MOUSEBUTTONDOWN
        || e->type == SDL_MOUSEBUTTONUP) {

        int x, y;
        SDL_GetMouseState(&x, &y);

        bool inside = true;
        if ((x < position.x) || (x > position.x + BUTTON_WIDTH)
            || (y < position.y) || (y > position.y + BUTTON_HEIGHT)) {

            inside = false;
        }

        if (! inside) {
            current_sprite = BUTTON_SPRITE_MOUSE_OUT;
        } else {
            switch (e->type) {
                case SDL_MOUSEMOTION:
                    current_sprite = BUTTON_SPRITE_MOUSE_OVER;
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    current_sprite = BUTTON_SPRITE_MOUSE_DOWN;
                    break;
                case SDL_MOUSEBUTTONUP:
                    current_sprite = BUTTON_SPRITE_MOUSE_UP;
                    break;
            }
        }
    }
}

void LButton::render()
{
    g_texture.render(position.x, position.y, &sprite_clips[current_sprite]);
}

void LButton::free()
{
    g_texture.free();
}
