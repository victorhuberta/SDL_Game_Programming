#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>

#include "LTexture.h"

bool init();
bool load_media();
void destroy();

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

const char* FONT_PATH = "font.ttf";
const int FONT_SIZE = 16;
const std::string TEXT_STR = "Hello, world!";

SDL_Window* g_window = NULL;
SDL_Renderer* g_renderer = NULL;
TTF_Font* g_font = NULL;
LTexture text;

int main(int argc, char* argv[])
{
    if (! init()) {
        printf("Failed to initialize!\n");
        return 1;
    }

    if (! load_media()) {
        printf("Failed to load media!\n");
        destroy();
        return 1;
    }

    bool quit = false;
    SDL_Event e;

    SDL_Color text_color = {0, 0, 0, 0xFF};
    std::string input_text = "Some Text";
    text.load_text_from_font(g_font, input_text, text_color);
    bool render_needed = false;

    SDL_StartTextInput();

    while (! quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            } else if (e.type == SDL_KEYDOWN) {
                if (e.key.keysym.sym == SDLK_BACKSPACE &&
                    input_text.length() > 0)
                {
                    input_text.pop_back();
                    render_needed = true;
                } else if (SDL_GetModState() & KMOD_CTRL)
                {
                    if (e.key.keysym.sym == SDLK_c) {
                        printf("Copy\n");
                        SDL_SetClipboardText(input_text.c_str());
                    } else if (e.key.keysym.sym == SDLK_v) {
                        printf("Paste\n");
                        input_text = SDL_GetClipboardText();
                        render_needed = true;
                    }
                }
            } else if (e.type == SDL_TEXTINPUT) {
                if (! (((e.text.text[0] == 'c' || e.text.text[0] == 'C') ||
                    (e.text.text[0] == 'v' || e.text.text[0] == 'V')) &&
                    ((SDL_GetModState() & KMOD_CTRL) == KMOD_CTRL)) )
                {
                    input_text += e.text.text;
                    render_needed = true;
                }
            }
        }

        if (render_needed) {
            if (input_text.length() != 0) {
                text.load_text_from_font(g_font, input_text, text_color);
            } else {
                text.load_text_from_font(g_font, " ", text_color);
            }
            render_needed = false;
        }

        SDL_SetRenderDrawColor(g_renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear(g_renderer);

        int x = (SCREEN_WIDTH - text.get_width()) / 2;
        int y = (SCREEN_HEIGHT - text.get_height()) / 2;
        text.render(x, y);

        SDL_RenderPresent(g_renderer);
    }

    SDL_StopTextInput();
    destroy();
    return 0;
}

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Failed to initialize SDL: %s\n", SDL_GetError());
        success = false;
    } else {
        
        g_window = SDL_CreateWindow("FONT DEMO", SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT,
            SDL_WINDOW_SHOWN);
        if (g_window == NULL) {
            printf("Failed to create window: %s\n", SDL_GetError());
            success = false;
        } else {

            g_renderer = SDL_CreateRenderer(g_window, -1,
                SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
            if (g_renderer == NULL) {
                printf("Failed to create renderer: %s\n", SDL_GetError());
                success = false;
            } else {

                if (TTF_Init() == -1) {
                    printf("Failed to initialize SDL_ttf: %s\n", TTF_GetError());
                    success = false;
                }
            }
        }
    }

    return success;
}

bool load_media()
{
    bool success = true;

    g_font = TTF_OpenFont(FONT_PATH, FONT_SIZE);
    if (g_font == NULL) {
        printf("Failed to open font: %s\n", TTF_GetError());
        success = false;
    } else {

        text.set_renderer(g_renderer);

        SDL_Color color = {0, 0, 0};
        if (! text.load_text_from_font(g_font, TEXT_STR, color)) {
            printf("Failed to load text from font!\n");
            success = false;
        }
    }

    return success;
}

void destroy()
{
    text.free();

    if (g_font != NULL) {
        TTF_CloseFont(g_font);
        g_font = NULL;
    }

    if (g_renderer != NULL) {
        SDL_DestroyRenderer(g_renderer);
        g_renderer = NULL;
    }

    if (g_window != NULL) {
        SDL_DestroyWindow(g_window);
        g_window = NULL;
    }

    TTF_Quit();
    SDL_Quit();
}
