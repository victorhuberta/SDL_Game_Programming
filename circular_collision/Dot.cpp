#include <SDL2/SDL.h>
#include <string>
#include "Dot.h"

Dot::Dot(SDL_Renderer* r, std::string path, int x_in, int y_in)
{
    dot_tex.set_renderer(r);
    if (! dot_tex.load_from_file(path)) {
        throw FailedToCreateDot {"Invalid path given"};
    }

    x = x_in;
    y = y_in;
    vel_x = 0;
    vel_y = 0;

    dot_circ.r = DOT_WIDTH / 2;
    dot_circ.x = x + dot_circ.r;
    dot_circ.y = y + dot_circ.r;
}
    

void Dot::handle_event(SDL_Event* e)
{
    if ((e->type == SDL_KEYDOWN) && (e->key.repeat == 0)) {
        switch (e->key.keysym.sym) {
            case SDLK_UP: vel_y -= DOT_VEL; break;
            case SDLK_DOWN: vel_y += DOT_VEL; break;
            case SDLK_LEFT: vel_x -= DOT_VEL; break;
            case SDLK_RIGHT: vel_x += DOT_VEL; break;
        }
    } else if ((e->type == SDL_KEYUP) && (e->key.repeat == 0)) {
        switch (e->key.keysym.sym) {
            case SDLK_UP: vel_y += DOT_VEL; break;
            case SDLK_DOWN: vel_y -= DOT_VEL; break;
            case SDLK_LEFT: vel_x += DOT_VEL; break;
            case SDLK_RIGHT: vel_x -= DOT_VEL; break;
        }
    }
}

void Dot::move(int max_x, int max_y, SDL_Rect* obj_rect, Dot& other)
{
    x += vel_x;
    dot_circ.x += vel_x;

    if ( (x < 0) || ((x + DOT_WIDTH) > max_x) || in_collision(obj_rect) ||
        collides_with(other) )
    {
        x -= vel_x;
        dot_circ.x -= vel_x;
    }

    y += vel_y;
    dot_circ.y += vel_y;

    if ( (y < 0) || ((y + DOT_HEIGHT) > max_y) || in_collision(obj_rect) ||
        collides_with(other) )
    {
        y -= vel_y;
        dot_circ.y -= vel_y;
    }
}

void Dot::render()
{
    dot_tex.render(x, y);
}

bool Dot::in_collision(SDL_Rect* obj_rect)
{
    int top = obj_rect->y, bottom = obj_rect->y + obj_rect->h;
    int left = obj_rect->x, right = obj_rect->x + obj_rect->w;

    int closest_x = (dot_circ.x < left) ? left :
        ( (dot_circ.x > right) ? right : dot_circ.x );

    int closest_y = (dot_circ.y < top) ? top :
        ( (dot_circ.y > bottom) ? bottom : dot_circ.y );

    return ( distance(dot_circ.x, dot_circ.y, closest_x, closest_y) <
        (dot_circ.r * dot_circ.r) );
}

bool Dot::collides_with(Dot& other)
{
    int total_radii = dot_circ.r + other.get_radius();
    return ( distance(dot_circ.x, dot_circ.y, other.get_x(), other.get_y()) <
        (total_radii * total_radii) );
}

int distance(int x1, int y1, int x2, int y2)
{
    int x_diff = x1 - x2, y_diff = y1 - y2;
    return (x_diff*x_diff + y_diff*y_diff);
}
