#ifndef _DOT_H
#define _DOT_H

#include <SDL2/SDL.h>
#include <exception>
#include <string>
#include "LTexture.h"

struct Circle
{
    int x, y;
    int r;
};

class Dot
{
    public:
        Dot(SDL_Renderer* r, std::string path, int x_in, int y_in);
        ~Dot() { dot_tex.free(); }

        static const int DOT_WIDTH = 20;
        static const int DOT_HEIGHT = 20;
        static const int DOT_VEL = 10;

        void handle_event(SDL_Event* e);
        void move(int max_x, int max_y, SDL_Rect* obj_rect, Dot& other);
        void render();
        bool in_collision(SDL_Rect* obj_rect);
        bool collides_with(Dot& other);

        int get_x() { return dot_circ.x; }
        int get_y() { return dot_circ.y; }
        int get_radius() { return dot_circ.r; }

    private:
        LTexture dot_tex;
        Circle dot_circ;

        int x, y;
        int vel_x, vel_y;
};

class FailedToCreateDot: public std::exception
{
    public:
        FailedToCreateDot(std::string m) : err_msg {m} {}

        virtual const char* what() const noexcept
        {
            return err_msg.c_str();
        }

    private:
        std::string err_msg;
};

int distance(int x1, int y1, int x2, int y2);

#endif
