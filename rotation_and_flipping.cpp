#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <exception>
#include <string>
#include <stdio.h>

class NoRenderer: public std::exception
{
    public:
        const char* what() const noexcept { return "No renderer specified."; }
} no_renderer;

class LTexture
{
    public:
        LTexture() : texture {NULL}, width {0}, height {0} {} 
        ~LTexture() { free(); }

        bool load_from_file(std::string path);
        void set_renderer(SDL_Renderer* r) { renderer = r; }
        void render(int x, int y, SDL_Rect* clip = NULL, \
            double angle = 0.0, SDL_Point* rotate_point = NULL,
            SDL_RendererFlip flip = SDL_FLIP_NONE);
        void free();

        int get_width() const { return width; }
        int get_height() const { return height; } 

    private:
        SDL_Texture* texture;
        SDL_Renderer* renderer;
        int width, height;
};

bool LTexture::load_from_file(std::string path)
{
    if (renderer == NULL) {
        throw no_renderer;
    }

    free();

    SDL_Texture* loaded_texture = NULL;
    SDL_Surface* loaded_surface = IMG_Load(path.c_str());
    if (loaded_surface == NULL) {
        printf("Failed to load image '%s': %s\n", path.c_str(), IMG_GetError());
    } else {

        SDL_SetColorKey(loaded_surface, SDL_TRUE,
            SDL_MapRGB(loaded_surface->format, 0x00, 0xFF, 0xFF));

        loaded_texture = SDL_CreateTextureFromSurface(renderer, loaded_surface);
        if (loaded_texture == NULL) {
            printf("Failed to create texture: %s\n", SDL_GetError());
        } else {
            width = loaded_surface->w;
            height = loaded_surface->h;
        }

        SDL_FreeSurface(loaded_surface);
    }

    texture = loaded_texture;
    return texture != NULL;
}

void LTexture::render(int x, int y, SDL_Rect* clip, double angle,
    SDL_Point* rotate_point, SDL_RendererFlip flip)
{
    if (renderer == NULL) {
        throw no_renderer;
    }

    SDL_Rect renderQuad {x, y, width, height};

    if (clip != NULL) {
        renderQuad.w = clip->w;
        renderQuad.h = clip->h;
    }

    SDL_RenderCopyEx(renderer, texture, clip, &renderQuad,
        angle, rotate_point, flip);
}

void LTexture::free()
{
    if (texture != NULL) {
        SDL_DestroyTexture(texture);
        texture = NULL;
        width = 0;
        height = 0;
    }
}

bool init();
void destroy();

const char* WINDOW_TITLE = "CLIP RENDERING";
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

SDL_Window* g_window = NULL;
SDL_Renderer* g_renderer = NULL;

const std::string ARROW_PATH = "arrow.png";
LTexture arrow;

int main(int argc, char* argv[])
{
    if (! init()) {
        printf("Failed to initialize!\n");
        destroy();
        return 1;
    }

    arrow.set_renderer(g_renderer);

    bool success = false;
    try {
        success = arrow.load_from_file(ARROW_PATH);
        if (! success) {
            printf("Failed to load arrow!\n");
            destroy();
            return 1;
        }
    } catch (NoRenderer& e) {
        printf("NoRenderer: %s\n", e.what());
        destroy();
        return 1;
    }

    bool quit = false;
    SDL_Event e;
    double angle = 0.0;
    SDL_RendererFlip flip = SDL_FLIP_NONE;

    while (! quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            } else if (e.type == SDL_KEYDOWN) {
                switch (e.key.keysym.sym) {
                    case SDLK_a:
                        angle -= 60;
                        break;
                    case SDLK_d:
                        angle += 60;
                        break;
                    case SDLK_q:
                        flip = SDL_FLIP_HORIZONTAL;
                        break;
                    case SDLK_w:
                        flip = SDL_FLIP_NONE;
                        break;
                    case SDLK_e:
                        flip = SDL_FLIP_VERTICAL;
                        break;
                }
            }
        }

        SDL_SetRenderDrawColor(g_renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear(g_renderer);

        arrow.render((SCREEN_WIDTH - arrow.get_width()) / 2,
            (SCREEN_HEIGHT - arrow.get_height()) / 2,
            NULL, angle, NULL, flip);

        SDL_RenderPresent(g_renderer);
    }

    return 0;
}

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Failed to initialize SDL: %s\n", SDL_GetError());
        success = false;
    } else {
        g_window = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT,
            SDL_WINDOW_SHOWN);
        if (g_window == NULL) {
            printf("Failed to create window: %s\n", SDL_GetError());
            success = false;
        } else {
            g_renderer = SDL_CreateRenderer(g_window, -1, SDL_RENDERER_ACCELERATED);
            if (g_renderer == NULL) {
                printf("Failed to create renderer: %s\n", SDL_GetError());
                success = false;
            }
        }
    }

    int img_flags = IMG_INIT_PNG;
    if ((IMG_Init(img_flags) & img_flags) != img_flags) {
        printf("Failed to initialize SDL_image: %s\n", IMG_GetError());
        success = false;
    }

    return success;
}

void destroy()
{
    arrow.free();

    if (g_renderer != NULL) {
        SDL_DestroyRenderer(g_renderer);
        g_renderer = NULL;
    }

    if (g_window != NULL) {
        SDL_DestroyWindow(g_window);
        g_window = NULL;
    }

    IMG_Quit();
    SDL_Quit();
}
