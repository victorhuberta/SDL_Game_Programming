#include <SDL2/SDL.h>
#include "VWindow.h"

bool init();
void destroy();

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

VWindow g_window {"WINDOW EVENTS", SCREEN_WIDTH, SCREEN_HEIGHT};

int main(int argc, char* argv[])
{
    if (! init()) {
        printf("Failed to initialize!\n");
        return 1;
    }

    bool quit = false;
    SDL_Event e;
    SDL_Color color = {0xFF, 0xFF, 0, 0xFF};

    while (! quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            }

            g_window.handle_event(e);
        }

        g_window.print_state();

        if (! g_window.is_minimized()) {
            int w = 300;
            int h = 400;
            g_window.draw_rect((SCREEN_WIDTH - w) / 2,
                (SCREEN_HEIGHT - h) / 2, w, h, color);
        }
    }

    destroy();
    return 0;
}

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Failed to initialize SDL2: %s\n", SDL_GetError());
        success = false;
    } else {

        if (! g_window.init()) {
            printf("Failed to initialize VWindow: %s\n", SDL_GetError());
            success = false;
        }
    }

    return success;
}

void destroy()
{
    g_window.destroy();
    SDL_Quit();
}
