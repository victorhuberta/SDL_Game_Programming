#include <SDL2/SDL.h>
#include <stdio.h>

bool init();
bool loadMedia(const char *imgName);
void destroy();

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const char IMG_NAME[] = "cat.bmp";

SDL_Window *gWindow = NULL;
SDL_Surface *gSurface = NULL;
SDL_Surface *imgSurface = NULL;

SDL_Event event;

int main(int argc, char **argv)
{
    bool quit = false;

    if (! init()) {
        printf("Failed to initialize!\n");
        return 1;
    }

    if (! loadMedia(IMG_NAME)) {
        printf("Failed to load media!\n");
        return 1;
    }

    while (! quit) {
        while (SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                quit = true;
                break;
            }
        }

        SDL_BlitSurface(imgSurface, NULL, gSurface, NULL);
        SDL_UpdateWindowSurface(gWindow);
    }

    return 0;
}

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Failed to initialize SDL: %s\n", SDL_GetError());
        success = false;
    } else {
        gWindow = SDL_CreateWindow("SDL Tutorial",
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            SCREEN_WIDTH, SCREEN_HEIGHT,
            SDL_WINDOW_SHOWN);

        if (gWindow == NULL) {
            printf("Failed to create gWindow: %s\n", SDL_GetError());
            success = false;
        } else {
            gSurface = SDL_GetWindowSurface(gWindow);
        }
    }

    return success;
}

bool loadMedia(const char *imgName)
{
    bool success = true;

    imgSurface = SDL_LoadBMP(imgName);
    if (imgSurface == NULL) {
        printf("Failed to load BMP image '%s': %s\n", imgName, SDL_GetError());
        success = false;
    }    

    return success;
}

void destroy()
{
    SDL_DestroyWindow(gWindow);
    gWindow = NULL;
    gSurface = NULL;

    SDL_FreeSurface(imgSurface);
    imgSurface = NULL;

    SDL_Quit();
}
