CC = g++
CFLAGS = -Wall
LFLAGS = -lSDL2 -lSDL2_image
SRC_FILES = keypresses_using_states.cpp
OUTPUT = output

all: $(SRC_FILES)
	$(CC) $(CFLAGS) $(LFLAGS) -o $(OUTPUT) $(SRC_FILES)
